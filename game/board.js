const { tile } = require('./tile');

const emptyBoard = {
  1.1: tile,
  1.2: tile,
  1.3: tile,
  2.1: tile,
  2.2: tile,
  2.3: tile,
  3.1: tile,
  3.2: tile,
  3.3: tile,
};

const isBoardEmpty = (board) => Object.values(board).reduce((a, b) => a && !b.contents.card, true);

const canPlaceCard = (board, location) => !board[location].contents.card;

const adjacentCoordinate = (direction) => {
  if (direction === 2) {
    return ['1', '3'];
  }
  return ['2'];
};

const findAdjacentCoordinates = (location) => {
  const [yCoord, xCoord] = location.split('.').map((cord) => parseInt(cord, 10));
  const xCords = adjacentCoordinate(xCoord).flatMap((x) => [yCoord].map((y) => `${y}.${x}`));
  const yCords = adjacentCoordinate(yCoord).flatMap((y) => [xCoord].map((x) => `${y}.${x}`));
  return Array.from(new Set(xCords.concat(yCords)));
};

module.exports = {
  isBoardEmpty, canPlaceCard, emptyBoard, findAdjacentCoordinates,
};
