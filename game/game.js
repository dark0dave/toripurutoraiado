const uuidv1 = require('uuid/v1');
const { isBoardEmpty, emptyBoard } = require('./board');
const { simple } = require('./rules');

const games = {};

const setUpGame = (playerOne) => {
  const board = JSON.parse(JSON.stringify(emptyBoard));
  const gameID = uuidv1();

  games[gameID] = {
    currentPlayer: '',
    playerOne,
    playerTwo: '',
    board,
  };

  return gameID;
};

const joinGame = (playerTwo, gameID) => {
  if (games[gameID] && !gameID[playerTwo]) {
    const currentPlayer = Math.random() > 0.5 ? games[gameID].playerOne : playerTwo;

    games[gameID].playerTwo = playerTwo;
    games[gameID].currentPlayer = currentPlayer;

    const nonRevealingPlayerIDTurn = currentPlayer === playerTwo ? "player two's" : "player one's";

    return `Game id is: ${gameID}, \n
      your id is ${playerTwo} keep this secret, \n
      player: ${nonRevealingPlayerIDTurn} goes first`;
  }
  return 'Game already started join can not join';
};

const turn = (gameID, input) => {
  let {
    currentPlayer,
    board,
  } = games[gameID];

  const {
    playerOne,
    playerTwo,
  } = games[gameID];

  if (isBoardEmpty(board)) {
    board = simple.resolve(board, currentPlayer, input.card, input.location);
    currentPlayer = currentPlayer === playerOne ? playerTwo : playerOne;
    return `Player: ${currentPlayer} turn`;
  }

  if (!games[gameID].winner) {
    const playerOneScore = board.contents.filter((card) => card.owner === playerOne);
    const playerTwoScore = 9 - playerOne;
    const winner = playerOneScore > playerTwoScore ? playerOne : playerTwo;
    games[gameID].winner = winner;
  }

  return `Player: ${games[gameID].winner} has won`;
};

module.exports = { setUpGame, turn, joinGame };
