const boardHelper = require('./board');

const simpleChangeOwner = (newCardLocation, newCard, adjacentCards) => {
  const [ny, nx] = newCardLocation.split('.').map((num) => parseInt(num, 10));

  const ownerChanged = adjacentCards.filter((cardAndCoord) => {
    const [y, x] = cardAndCoord.coord.split('.').map((num) => parseInt(num, 10));
    // left
    if (x > nx && y === ny) {
      return newCard.right > cardAndCoord.card.left;
    }
    // right
    if (x < nx && y === ny) {
      return newCard.left > cardAndCoord.card.right;
    }
    // top
    if (x === nx && y > ny) {
      return newCard.top > cardAndCoord.card.bottom;
    }
    // bottom
    if (x === nx && y < ny) {
      return newCard.bottom > cardAndCoord.card.top;
    }
    return false;
  });

  return ownerChanged;
};

const simpleResolve = (board, playerID, newCard, newCardLocation) => {
  if (!boardHelper.canPlaceCard(board, newCardLocation)) {
    throw new Error('Card can not be placed here');
  }
  const newBoard = JSON.parse(JSON.stringify(board));

  newBoard[newCardLocation].contents.card = newCard;
  newBoard[newCardLocation].contents.owner = playerID;

  const adjacentCards = boardHelper.findAdjacentCoordinates(newCardLocation)
    .filter((coord) => board[coord].contents.card)
    .map((coord) => ({ card: board[coord].contents.card, coord }));

  if (adjacentCards.length < 1) {
    return newBoard;
  }

  simpleChangeOwner(newCardLocation, newCard, adjacentCards).forEach((cardAndCoord) => {
    newBoard[cardAndCoord.coord].contents.owner = playerID;
  });

  return newBoard;
};

const rules = {
  simple: {
    resolve: simpleResolve,
    changeOwner: simpleResolve,
  },
};

module.exports = rules;
