# Triple Triad

[Triple Triad](https://finalfantasy.fandom.com/wiki/Triple_Triad) (トリプルトライアード, Toripuru Toraiādo?) is a card game originating from Final Fantasy VIII.
Here we present a version just for fun, no copywrite issues intended.

## Developing

You will need:
* [npm](https://www.npmjs.com/), node package manager
* [nodejs 12.10.0](https://nodejs.org/en/), latest version of nodejs
* [nvm](https://github.com/nvm-sh/nvm), for managing node versions
* [pre-commit](https://pre-commit.com/hooks.html), to prevent garbage being added to the repo
