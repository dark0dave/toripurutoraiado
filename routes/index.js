const uuidv1 = require('uuid/v1');
const express = require('express');
const { setUpGame, joinGame } = require('../game/game');

const router = express.Router();

// eslint-disable-next-line no-unused-vars
router.get('/', (req, res, next) => {
  res.render('index', { title: 'Triple Triad' });
});

// eslint-disable-next-line no-unused-vars
router.get('/game/startNewGame', (req, res) => {
  const playerOne = uuidv1();
  const gameID = setUpGame(playerOne);
  res.send(`Game id is: ${gameID}, share this with your opponent \n
   Your id is ${playerOne}, keep this secret for your moves`);
});

router.get('/game/*/join', (req, res) => {
  const gameID = req.params['0'];
  res.send(joinGame(uuidv1(), gameID));
});

router.post('/game/*/playCard', (req, res) => {
  const gameID = req.params['0'];
  const input = req.body;
  res.send(`GameID: ${gameID}, Card played: ${input}`);
});

module.exports = router;
