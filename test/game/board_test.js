const { expect } = require('chai');
const {
  emptyBoard,
  findAdjacentCoordinates,
  isBoardEmpty,
  canPlaceCard,
} = require('../../game/board');

const testValues = [
  { name: 'top left 1.1', testValue: '1.1', expectedResult: ['1.2', '2.1'] },
  { name: 'top middle 1.2', testValue: '1.2', expectedResult: ['1.1', '1.3', '2.2'] },
  { name: 'top right 1.3', testValue: '1.3', expectedResult: ['1.2', '2.3'] },
  { name: 'middle left 2.1', testValue: '2.1', expectedResult: ['1.1', '2.2', '3.1'] },
  { name: 'center 2.2', testValue: '2.2', expectedResult: ['1.2', '2.1', '2.3', '3.2'] },
  { name: 'middle right 2.3', testValue: '2.3', expectedResult: ['1.3', '2.2', '3.3'] },
  { name: 'bottom left 3.1', testValue: '3.1', expectedResult: ['2.1', '3.2'] },
  { name: 'bottom center 3.2', testValue: '3.2', expectedResult: ['2.2', '3.1', '3.3'] },
  { name: 'bottom left 3.3', testValue: '3.3', expectedResult: ['3.2', '2.3'] },
];

describe('findAdjacentCoordinates', () => {
  testValues.forEach((entry) => {
    it(`finds the correct locations for ${entry.name}`, () => {
      const coordinates = findAdjacentCoordinates(entry.testValue);
      expect(coordinates).to.include.members(entry.expectedResult);
    });
  });
});

describe('isBoardEmpty', () => {
  it('should be empty, when empty', () => {
    const testBoard = JSON.parse(JSON.stringify(emptyBoard));
    const isEmpty = isBoardEmpty(testBoard) === true;
    expect(isEmpty).to.be.equal(true);
  });

  it('should not be empty, when populated', () => {
    const testBoard = JSON.parse(JSON.stringify(emptyBoard));
    testBoard['1.1'].contents.card = 1;
    const isNotEmpty = isBoardEmpty(testBoard) === true;
    expect(isNotEmpty).to.be.equal(false);
  });
});

describe('canPlaceCard', () => {
  it('should able to place card when tile is empty empty', () => {
    const testBoard = JSON.parse(JSON.stringify(emptyBoard));
    const placeable = canPlaceCard(testBoard, '1.1') === true;
    expect(placeable).to.be.equal(true);
  });
});
