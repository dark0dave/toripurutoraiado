const { expect } = require('chai');
const { emptyBoard } = require('../../game/board');
const card = require('../../game/card');
const rules = require('../../game/rules');

describe('simple resolve', () => {
  let simpleBoard = JSON.parse(JSON.stringify(emptyBoard));
  const playerOneID = '1';
  const playerTwoID = '2';
  const cardOne = JSON.parse(JSON.stringify(card));
  const cardTwo = JSON.parse(JSON.stringify(card));
  const cardThree = JSON.parse(JSON.stringify(card));
  cardTwo.left = 2;
  const cardOneLocation = '1.2';
  const cardTwoLocation = '1.3';
  const cardThreeLocation = '2.1';

  before(() => {
    simpleBoard = rules.simple.resolve(simpleBoard, playerOneID, cardOne, cardOneLocation);
  });

  it('places a card on the empty board correctly', () => {
    expect(simpleBoard['1.2'].contents.card).to.be.equal(cardOne);
  });

  it('places the next card correctly', () => {
    simpleBoard = rules.simple.resolve(simpleBoard, playerTwoID, cardTwo, cardTwoLocation);
    expect(simpleBoard['1.3'].contents.card).to.be.equal(cardTwo);
  });

  it('changed ownership of first card, if stronger card is placed', () => {
    expect(simpleBoard['1.2'].contents.owner).to.be.equal(playerTwoID);
  });

  it('does not change ownership of first card, if weaker card is placed', () => {
    simpleBoard = rules.simple.resolve(simpleBoard, playerOneID, cardThree, cardThreeLocation);
    expect(simpleBoard['1.2'].contents.owner).to.be.equal(playerTwoID);
  });
});
